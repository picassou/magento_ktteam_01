<?php
namespace Test\HW;
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::THEME,
    'frontend/test/hw',
    __DIR__
);