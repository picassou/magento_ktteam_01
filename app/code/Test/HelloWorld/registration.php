<?php
namespace Test\HelloWorld;
use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
   ComponentRegistrar::MODULE,
    'Test_HelloWorld',
    __DIR__
);