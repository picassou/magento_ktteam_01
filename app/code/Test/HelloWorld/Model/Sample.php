<?php

namespace Test\HelloWorld\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;

class Sample extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'test_helloworld_sample';

    protected $_cacheTag = 'test_helloworld_sample';

    protected $_eventPrefix = 'test_helloworld_sample';

    protected function _construct()
    {
        $this->_init('Test\HelloWorld\Model\ResourceModel\Sample');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}