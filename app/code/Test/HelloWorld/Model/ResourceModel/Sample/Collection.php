<?php

namespace Test\HelloWorld\Model\ResourceModel\Sample;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'test_helloworld_sample_collection';
    protected $_eventObject = 'sample_collection';

    protected function _construct()
    {
        $this->_init('Test\HelloWorld\Model\Sample', 'Test\HelloWorld\Model\ResourceModel\Sample');
    }

}
