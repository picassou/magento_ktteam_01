<?php

namespace Test\HelloWorld\Controller\Index;

use  Magento\Framework\App\Action\Context;
use  Magento\Framework\View\Result\PageFactory;
use  Test\HelloWorld\Model\SampleFactory;
use  Magento\Framework\App\Action\Action;

class Index extends Action
{
    protected $_PageFactory;
    protected $_SampleFactory;

    public function __construct(Context $context, PageFactory $PageFactory,  SampleFactory $SampleFactory)
    {
        $this->_PageFactory = $PageFactory;
        $this->_SampleFactory = $SampleFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->_PageFactory->create();
        $sample = $this->_SampleFactory->create();
        $sample->load(1);
        echo '<pre>';
        foreach ($sample->getData() as $item){
            echo $item.'; ';
        }
        echo '</pre>';
        return $resultPage;
    }
}

?>