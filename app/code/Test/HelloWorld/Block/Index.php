<?php

namespace Test\HelloWorld\Block;

use  Magento\Framework\View\Element\Template;
use  Magento\Framework\View\Element\Template\Context;
use  Test\HelloWorld\Model\SampleFactory;

class Index extends Template
{
    protected $_SampleFactory;

    public function __construct(Context $context,  SampleFactory $SampleFactory)
    {
        $this->_SampleFactory = $SampleFactory;
        parent::__construct($context);
    }

    public function getSampleCollection()
    {
        $page     = $this->getRequest()->getParam('p', 1);
        $pageSize = $this->getRequest()->getParam('limit', 20);

        $sample           = $this->_SampleFactory->create();
        $sampleCollection = $sample->getCollection();

        $sampleCollection->addFieldToFilter('status', 1);
        $sampleCollection->setOrder('id', 'ASC');
        $sampleCollection->setPageSize($pageSize);
        $sampleCollection->setCurPage($page);

        return $sampleCollection;
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $this->pageConfig->getTitle()->set(__('HELLO_WORLD'));

        if ($this->getSampleCollection()) {
            $pagination = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'test.helloworld.samples')
                ->setAvailableLimit([20=>20])
                ->setShowPerPage(true)
                ->setCollection($this->getSampleCollection());

            $this->setChild('pagination', $pagination);
            $this->getSampleCollection()->load();
        }
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pagination');
    }

    public function getContent()
    {
        return __('HELLO_WORLD');
    }
}